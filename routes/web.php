<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(session('isLoggedIn')) {
		return redirect('pokemon');
    } else {
		return redirect('login');
	}
});

// pokemon
Route::get('pokemon', 'Pokemon@index')->middleware('isAuthComplete');
Route::get('pokemon/add', 'Pokemon@add')->middleware('isAuthComplete');
Route::get('pokemon/details/{id}', 'Pokemon@details')->middleware('isAuthComplete');
Route::post('pokemon/save', 'Pokemon@save');
Route::get('pokemon/edit/{id}', 'Pokemon@edit')->middleware('isAuthComplete');
Route::post('pokemon/update', 'Pokemon@update');
Route::post('pokemon/delete', 'Pokemon@delete')->middleware('isAuthComplete');

// user
Route::get('user', 'User@index')->middleware('isAuthComplete');
Route::get('user/add', 'User@add')->middleware('isAuthComplete');
Route::post('user/save', 'User@save');

Route::get('login', 'User@login');
Route::post('login/process', 'User@loginProcess');
Route::get('logout', 'User@logout');
