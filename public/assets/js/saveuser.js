$('body').on('submit', '#saveUser', function(event){
	event.preventDefault();
	var formData = new FormData(this);

	if($('#email').val() == '') {
		alert('Email is empty!');
	} else if($('#name').val() == '') {
		alert('Name is empty!');
	} else if($('#password').val() == '') {
		alert('Password is empty!');
	} else if($('#confirm_password').val() == '') {
		alert('Password confirmation is empty!');
	} else if($('#password').val() != $('#confirm_password').val()) {
		alert('Wrong password confirmation!');
	} else {
		saveUser(formData);
	}

});

function saveUser(formData) {
	$.ajax({
		url: '/user/save',
		type: 'post',
		processData: false,
		contentType: false,
		data: formData,
		beforeSend: function(){
		},
		success: function(data) {
			if(data.message == 'success') {
				window.location.replace('/');
			} else if(data.message == 'session_expired') {
				window.location.replace('/');
			} else {
				$('#loginMessage').addClass('alert alert-danger');
				$('#loginMessage').html(data.message);
			}
		},
		error: function() {
			$('#loginMessage').addClass('alert alert-danger');
			$('#loginMessage').html('Unable to get data');
  		}
  	});
}
