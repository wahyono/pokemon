$('body').on('click', '#deletePokemon', function(){
	deletePokemon($(this).data('reference'));
});

function deletePokemon(reference, token) {
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

	$.ajax({
		url: '/pokemon/delete',
		type: 'post',
		data: {
      reference: reference
    },
		beforeSend: function(){
		},
		success: function(data) {
			if(data.message == 'success') {
				window.location.replace(data.redirect);
			} else if(data.message == 'session_expired') {
				window.location.replace('/');
			} else if(data.message == 'data_not_found') {
				window.location.replace(data.redirect);
			} else {
				$('#loginMessage').addClass('alert alert-danger');
				$('#loginMessage').html(data.message);
			}
		},
		error: function() {
			$('#loginMessage').addClass('alert alert-danger');
			$('#loginMessage').html('Unable to get data');
  		}
  	});
}
