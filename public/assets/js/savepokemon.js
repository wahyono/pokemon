$('body').on('submit', '#savePokemon', function(event){
	event.preventDefault();
	var formData = new FormData(this);

	if($('#generation').val() == '') {
		alert('Generation is empty!');
	} else if($('#name').val() == '') {
		alert('Name is empty!');
	} else {
		savePokemon(formData);
	}

});

function savePokemon(formData) {
	$.ajax({
		url: '/pokemon/save',
		type: 'post',
		processData: false,
		contentType: false,
		data: formData,
		beforeSend: function(){
		},
		success: function(data) {
			if(data.message == 'success') {
				window.location.replace('/pokemon');
			} else if(data.message == 'session_expired') {
				window.location.replace('/');
			} else {
				$('#loginMessage').addClass('alert alert-danger');
				$('#loginMessage').html(data.message);
			}
		},
		error: function() {
			$('#loginMessage').addClass('alert alert-danger');
			$('#loginMessage').html('Unable to get data');
  		}
  	});
}
