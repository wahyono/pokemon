$('body').on('submit', '#form-signin', function(event){
	event.preventDefault();
	var formData = new FormData(this);
	if($('#inputEmail').val() != '' && $('#inputPassword').val() != '') {
			login(formData);
	} else {
		$('#loginMessage').addClass('alert alert-danger');
		$('#loginMessage').html('Complete these fields!');
	}
});

var host = window.location.hostname;
var redHost = null;
if(host == 'localhost') {
	redHost = 'public';
}

function login(formData) {
	$.ajax({
		url: '/login/process',
		type: 'post',
		processData: false,
		contentType: false,
		data: formData,
		beforeSend: function(){
		},
		success: function(data) {
			if(data.code == '003') {
				$('#loginMessage').removeClass('alert alert-danger');
				$('#loginMessage').html('Please wait...');
				window.location.replace('/pokemon');
			} else if(data.code == '002') {
				$('#loginMessage').addClass('alert alert-danger');
				$('#loginMessage').html(data.message);
			} else if(data.code == '001') {
				$('#loginMessage').addClass('alert alert-danger');
				$('#loginMessage').html(data.message);
			} else {
				$('#loginMessage').addClass('alert alert-danger');
				$('#loginMessage').html('Unable to process data');
			}
		},
		error: function() {
			$('#loginMessage').addClass('alert alert-danger');
			$('#loginMessage').html('Unable to get data');
  		}
  	});
}
