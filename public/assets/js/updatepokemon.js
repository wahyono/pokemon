$('body').on('submit', '#updatePokemon', function(event){
	event.preventDefault();
	var formData = new FormData(this);
	
	if($('#generation').val() == '') {
		alert('Generation is empty!');
	} else if($('#name').val() == '') {
		alert('Name is empty!');
	} else {
		updatePokemon(formData);
	}
});

function updatePokemon(formData) {
	$.ajax({
		url: '/pokemon/update',
		type: 'post',
		processData: false,
		contentType: false,
		data: formData,
		beforeSend: function(){
		},
		success: function(data) {
			if(data.message == 'success') {
				window.location.replace(data.redirect);
			} else if(data.message == 'session_expired') {
				window.location.replace('/');
			} else if(data.message == 'data_not_found') {
				window.location.replace(data.redirect);
			} else {
				$('#loginMessage').addClass('alert alert-danger');
				$('#loginMessage').html(data.message);
			}
		},
		error: function() {
			$('#loginMessage').addClass('alert alert-danger');
			$('#loginMessage').html('Unable to get data');
  		}
  	});
}
