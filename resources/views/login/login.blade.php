<!doctype html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
	    <title>POKEMON DATABASE</title>
	    <link href="{{ URL::asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
	    <link href="{{ URL::asset('assets/css/login.css') }}" rel="stylesheet">
		<script src="{{ URL::asset('assets/jquery/jquery.js') }}" type="text/javascript"></script>

		<style>
			body {
				background-color: #FFFFFF;
			}
		</style>
	</head>

	<body class="text-center">
		<form class="form-signin" id="form-signin">
			{{ csrf_field() }}
			<img class="mb-4" src="{{ URL::asset('assets/images/logopokemon.png') }}" alt="" width="150" height="150">
			<h1 class="h3 mb-3 font-weight-normal" id="loginMessage">Please sign in</h1>
			<label for="inputEmail" class="sr-only">Email address</label>
			<input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required>
			<label for="inputPassword" class="sr-only">Password</label>
			<input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
			<button class="btn btn-lg btn-primary btn-block" type="submit" id="buttonSignIn">Sign in</button>
			<p class="mt-5 mb-3 text-muted">&copy; 2019</p>
		</form>

		<script src="{{ URL::asset('assets/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
		<script src="{{ URL::asset('assets/js/login.js') }}" type="text/javascript"></script>

	</body>
</html>
