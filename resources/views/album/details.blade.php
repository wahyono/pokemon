
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
    <title>POKEMON DATABASE</title>
    <link href="{{ URL::asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ URL::asset('assets/css/album.css') }}" rel="stylesheet">
  </head>

  <body>

    @include('parts/header')

    <main role="main">
      <div class="album py-5 bg-light">
        <div class="container">

          <div class="row">
			  <div class="col-md-12 text-center">
				  <img src="{{ $data['photo'] }}" width="300">
			  </div>
          </div>
		  <div class="row">
			  <div class="col-md-12 text-center" style="margin-top: 30px;">
				  <a href="{{ url('pokemon/edit/'.$data['id']) }}" class="btn btn-primary">Edit</a>
				  <a href="javascript:void(0)" class="btn btn-primary">Add family</a>
				  <a href="javascript:void(0)" class="btn btn-danger" id="deletePokemon" data-reference="{{ $data['id'] }}">Delete</a>
			  </div>
          </div>
		  <div class="row">
			  <div class="col-md-12">
				  <div class="card" style="margin-top: 30px;">
					  <div class="card-header">
						  Profile
					  </div>
					  <div class="card-body">
						  <div>
							  <h5 class="card-title">Name</h5>
							  <p class="card-text">{{ $data['details']->name }}</p>
						  </div>
						  <div style="margin-top: 15px;">
							  <h5 class="card-title">Alias</h5>
							  <p class="card-text">{{ $data['alias']}}</p>
						  </div>
						  <div>
							  <h5 class="card-title">Generation</h5>
							  <p class="card-text">{{ $data['details']->generation }}</p>
						  </div>
					  </div>
				  </div>
			  </div>
		  </div>
		  @if(count($data['type']) > 0)
		  <div class="row">
			  <div class="col-md-12">
				  <div class="card" style="margin-top: 30px;">
					  <div class="card-header">
						  Type
					  </div>
					  <div class="card-body">
						  @foreach($data['type'] as $type)
						  <button type="button" class="btn btn-primary">{{ $type }}</button>
						  @endforeach
					  </div>
				  </div>
			  </div>
		  </div>
		  @endif
		  <div class="row">
			  <div class="col-md-6">
				  <div class="row">
					  <div class="col-md-12">
						  <div class="card" style="margin-top: 30px;">
							  <div class="card-header">
							    Stats
							  </div>
							  <ul class="list-group list-group-flush">
								  <li class="list-group-item d-flex justify-content-between align-items-center">
									  Attack
									  <span class="badge badge-primary badge-pill">{{ $data['details']->attack }}</span>
								  </li>
								  <li class="list-group-item d-flex justify-content-between align-items-center">
								    Defense
								    <span class="badge badge-primary badge-pill">{{ $data['details']->defense }}</span>
								  </li>
								  <li class="list-group-item d-flex justify-content-between align-items-center">
								    Stamina
								    <span class="badge badge-primary badge-pill">{{ $data['details']->stamina }}</span>
								  </li>
							  </ul>
						  </div>
					  </div>
					  <div class="col-md-12">
						  <div class="card" style="margin-top: 30px;">
							  <div class="card-header">
								  Descriptions
							  </div>
							  <div class="card-body">
								  <p class="card-text">{{ $data['details']->descriptions }}</p>
							  </div>
						  </div>
					  </div>
				  </div>
			  </div>
			  <div class="col-md-6">
				  <div class="row">
					  @if(count($data['weakness']) > 0)
					  <div class="col-md-12">
						  <div class="card" style="margin-top: 30px;">
							  <div class="card-header">
							    Weakness
							  </div>
							  <ul class="list-group list-group-flush">
								  @foreach($data['weakness'] as $weakness)
								  <li class="list-group-item d-flex justify-content-between align-items-center">
									  {{ $weakness }}
								  </li>
								  @endforeach
							  </ul>
						  </div>
					  </div>
					  @endif
					  @if(count($data['resistances']) > 0)
					  <div class="col-md-12">
						  <div class="card" style="margin-top: 30px;">
							  <div class="card-header">
							    Resistances
							  </div>
							  <ul class="list-group list-group-flush">
								  @foreach($data['resistances'] as $resistances)
								  <li class="list-group-item d-flex justify-content-between align-items-center">
									  {{ $resistances }}
								  </li>
								  @endforeach
							  </ul>
						  </div>
					  </div>
					  @endif
				  </div>
			  </div>
          </div>
        </div>
      </div>

    </main>

    @include('parts/footer')

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ URL::asset('assets/jquery/jquery.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/album/js/popper.min.js') }}"></script>
    <script src="{{ URL::asset('assets/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/album/js/holder.min.js') }}"></script>
	<script src="{{ URL::asset('assets/js/deletepokemon.js') }}" type="text/javascript"></script>
  </body>
</html>
