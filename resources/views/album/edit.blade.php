
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
    <title>POKEMON DATABASE</title>
    <link href="{{ URL::asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('assets/select2/css/select2.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ URL::asset('assets/css/album.css') }}" rel="stylesheet">
  </head>

  <body>

    @include('parts/header')

    <main role="main">

      <section class="jumbotron text-center">
        <div class="container">
          <img class="d-block mx-auto mb-4" src="{{ URL::asset('assets/images/logopokemon.png') }}" alt="" width="150" height="150">
        </div>
      </section>

      <div class="album py-5 bg-light">
        <div class="container">

          <div class="row">
            <div class="col-md-12">
              <a href="{{ url('pokemon/details/'.$data['id']) }}" class="btn btn-primary">Back to details</a>
            </div>
          </div>

			<form id="updatePokemon" enctype="multipart/form-data" style="margin-top: 30px;">
				{{ csrf_field() }}
        <input type="hidden" name="reference" value="{{ $data['id'] }}">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Generation</label>
							<select class="form-control select2" name="generation" id="generation">
								<option></option>
								<option value="Generation I" <?php if($data['details']->generation == 'Generation I') { echo 'selected'; } ?> >Generation I</option>
								<option value="Generation II" <?php if($data['details']->generation == 'Generation II') { echo 'selected'; } ?> >Generation II</option>
								<option value="Generation III" <?php if($data['details']->generation == 'Generation III') { echo 'selected'; } ?> >Generation III</option>
								<option value="Generation IV" <?php if($data['details']->generation == 'Generation IV') { echo 'selected'; } ?> >Generation IV</option>
								<option value="Generation V" <?php if($data['details']->generation == 'Generation V') { echo 'selected'; } ?> >Generation V</option>
								<option value="Generation VI" <?php if($data['details']->generation == 'Generation VI') { echo 'selected'; } ?> >Generation VI</option>
								<option value="Generation VVI" <?php if($data['details']->generation == 'Generation VII') { echo 'selected'; } ?> >Generation VII</option>
							</select>
						</div>
					</div>
	            </div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>Name</label>
							<input type="text" class="form-control" name="name" placeholder="Name" value="{{ $data['details']->name }}" id="name">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Alias</label>
							<input type="text" class="form-control" name="alias" placeholder="Alias" value="{{ $data['details']->value }}">
						</div>
					</div>
	            </div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Type</label>
							<select class="form-control select2" name="type[]" multiple>
								<option value="Bug" <?php if(in_array('Bug', $data['type'])) { echo 'selected'; } ?> >Bug</option>
								<option value="Dark" <?php if(in_array('Dark', $data['type'])) { echo 'selected'; } ?> >Dark</option>
								<option value="Dragon" <?php if(in_array('Dragon', $data['type'])) { echo 'selected'; } ?> >Dragon</option>
								<option value="Electric" <?php if(in_array('Electric', $data['type'])) { echo 'selected'; } ?> >Electric</option>
								<option value="Fairy" <?php if(in_array('Fairy', $data['type'])) { echo 'selected'; } ?> >Fairy</option>
								<option value="Fighting" <?php if(in_array('Fighting', $data['type'])) { echo 'selected'; } ?> >Fighting</option>
								<option value="Fire" <?php if(in_array('Fire', $data['type'])) { echo 'selected'; } ?> >Fire</option>
								<option value="Flying" <?php if(in_array('Flying', $data['type'])) { echo 'selected'; } ?> >Flying</option>
								<option value="Fighting" <?php if(in_array('Fighting', $data['type'])) { echo 'selected'; } ?> >Fighting</option>
								<option value="Ghost" <?php if(in_array('Ghost', $data['type'])) { echo 'selected'; } ?> >Ghost</option>
								<option value="Grass" <?php if(in_array('Grass', $data['type'])) { echo 'selected'; } ?> >Grass</option>
								<option value="Ground" <?php if(in_array('Ground', $data['type'])) { echo 'selected'; } ?> >Ground</option>
								<option value="Ice" <?php if(in_array('Ice', $data['type'])) { echo 'selected'; } ?> >Ice</option>
								<option value="Normal" <?php if(in_array('Normal', $data['type'])) { echo 'selected'; } ?> >Normal</option>
								<option value="Poison" <?php if(in_array('Poison', $data['type'])) { echo 'selected'; } ?> >Poison</option>
								<option value="Psychic" <?php if(in_array('Psychic', $data['type'])) { echo 'selected'; } ?> >Psychic</option>
								<option value="Rock" <?php if(in_array('Rock', $data['type'])) { echo 'selected'; } ?> >Rock</option>
								<option value="Steel" <?php if(in_array('Steel', $data['type'])) { echo 'selected'; } ?> >Steel</option>
								<option value="Water" <?php if(in_array('Water', $data['type'])) { echo 'selected'; } ?> >Water</option>
							</select>
						</div>
					</div>
	            </div>
				<div class="row">
					<div class="col-md-2">
						<div class="form-group">
							<label>Attack</label>
							<input type="number" class="form-control" name="attack" placeholder="Attack" value="{{ $data['details']->attack }}">
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label>Defense</label>
							<input type="number" class="form-control" name="defense" placeholder="Defense" value="{{ $data['details']->defense }}">
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label>Stamina</label>
							<input type="number" class="form-control" name="stamina" placeholder="Stamina" value="{{ $data['details']->stamina }}">
						</div>
					</div>
	            </div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Weakness</label>
							<select class="form-control select2" name="weakness[]" multiple>
								<option value="Bug" <?php if(in_array('Bug', $data['weakness'])) { echo 'selected'; } ?> >Bug</option>
								<option value="Dark" <?php if(in_array('Dark', $data['weakness'])) { echo 'selected'; } ?> >Dark</option>
								<option value="Dragon" <?php if(in_array('Dragon', $data['weakness'])) { echo 'selected'; } ?> >Dragon</option>
								<option value="Electric" <?php if(in_array('Electric', $data['weakness'])) { echo 'selected'; } ?> >Electric</option>
								<option value="Fairy" <?php if(in_array('Fairy', $data['weakness'])) { echo 'selected'; } ?> >Fairy</option>
								<option value="Fighting" <?php if(in_array('Fighting', $data['weakness'])) { echo 'selected'; } ?> >Fighting</option>
								<option value="Fire" <?php if(in_array('Fire', $data['weakness'])) { echo 'selected'; } ?> >Fire</option>
								<option value="Flying" <?php if(in_array('Flying', $data['weakness'])) { echo 'selected'; } ?> >Flying</option>
								<option value="Fighting" <?php if(in_array('Fighting', $data['weakness'])) { echo 'selected'; } ?> >Fighting</option>
								<option value="Ghost" <?php if(in_array('Ghost', $data['weakness'])) { echo 'selected'; } ?> >Ghost</option>
								<option value="Grass" <?php if(in_array('Grass', $data['weakness'])) { echo 'selected'; } ?> >Grass</option>
								<option value="Ground" <?php if(in_array('Ground', $data['weakness'])) { echo 'selected'; } ?> >Ground</option>
								<option value="Ice" <?php if(in_array('Ice', $data['weakness'])) { echo 'selected'; } ?> >Ice</option>
								<option value="Normal" <?php if(in_array('Normal', $data['weakness'])) { echo 'selected'; } ?> >Normal</option>
								<option value="Poison" <?php if(in_array('Poison', $data['weakness'])) { echo 'selected'; } ?> >Poison</option>
								<option value="Psychic" <?php if(in_array('Psychic', $data['weakness'])) { echo 'selected'; } ?> >Psychic</option>
								<option value="Rock" <?php if(in_array('Rock', $data['weakness'])) { echo 'selected'; } ?> >Rock</option>
								<option value="Steel" <?php if(in_array('Steel', $data['weakness'])) { echo 'selected'; } ?> >Steel</option>
								<option value="Water" <?php if(in_array('Water', $data['weakness'])) { echo 'selected'; } ?> >Water</option>
							</select>
						</div>
					</div>
	            </div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Resistance</label>
							<select class="form-control select2" name="resistances[]" multiple>
								<option value="Bug" <?php if(in_array('Bug', $data['resistances'])) { echo 'selected'; } ?> >Bug</option>
								<option value="Dark" <?php if(in_array('Dark', $data['resistances'])) { echo 'selected'; } ?> >Dark</option>
								<option value="Dragon" <?php if(in_array('Dragon', $data['resistances'])) { echo 'selected'; } ?> >Dragon</option>
								<option value="Electric" <?php if(in_array('Electric', $data['resistances'])) { echo 'selected'; } ?> >Electric</option>
								<option value="Fairy" <?php if(in_array('Fairy', $data['resistances'])) { echo 'selected'; } ?> >Fairy</option>
								<option value="Fighting" <?php if(in_array('Fighting', $data['resistances'])) { echo 'selected'; } ?> >Fighting</option>
								<option value="Fire" <?php if(in_array('Fire', $data['resistances'])) { echo 'selected'; } ?> >Fire</option>
								<option value="Flying" <?php if(in_array('Flying', $data['resistances'])) { echo 'selected'; } ?> >Flying</option>
								<option value="Fighting" <?php if(in_array('Fighting', $data['resistances'])) { echo 'selected'; } ?> >Fighting</option>
								<option value="Ghost" <?php if(in_array('Ghost', $data['resistances'])) { echo 'selected'; } ?> >Ghost</option>
								<option value="Grass" <?php if(in_array('Grass', $data['resistances'])) { echo 'selected'; } ?> >Grass</option>
								<option value="Ground" <?php if(in_array('Ground', $data['resistances'])) { echo 'selected'; } ?> >Ground</option>
								<option value="Ice" <?php if(in_array('Ice', $data['resistances'])) { echo 'selected'; } ?> >Ice</option>
								<option value="Normal" <?php if(in_array('Normal', $data['resistances'])) { echo 'selected'; } ?> >Normal</option>
								<option value="Poison" <?php if(in_array('Poison', $data['resistances'])) { echo 'selected'; } ?> >Poison</option>
								<option value="Psychic" <?php if(in_array('Psychic', $data['resistances'])) { echo 'selected'; } ?> >Psychic</option>
								<option value="Rock" <?php if(in_array('Rock', $data['resistances'])) { echo 'selected'; } ?> >Rock</option>
								<option value="Steel" <?php if(in_array('Steel', $data['resistances'])) { echo 'selected'; } ?> >Steel</option>
								<option value="Water" <?php if(in_array('Water', $data['resistances'])) { echo 'selected'; } ?> >Water</option>
							</select>
						</div>
					</div>
	            </div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Descriptions</label>
							<textarea class="form-control" name="descriptions" placeholder="Descriptions">{{ $data['details']->descriptions }}</textarea>
						</div>
					</div>
	            </div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Photo</label>
							<input type="file" name="photo" class="form-control">
						</div>
					</div>
	            </div>
				<div class="row">
					<div class="col-md-6">
						<button class="btn btn-primary btn-lg btn-block" type="submit">Update this Pokemon</button>
					</div>
	            </div>
			</form>
        </div>
      </div>

    </main>

    @include('parts/footer')

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ URL::asset('assets/jquery/jquery.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/album/js/popper.min.js') }}"></script>
    <script src="{{ URL::asset('assets/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::asset('assets/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/album/js/holder.min.js') }}"></script>
	<script src="{{ URL::asset('assets/js/updatepokemon.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		$('.select2').select2({
			placeholder: "Please choose!",
    		allowClear: true
		});
	</script>
  </body>
</html>
