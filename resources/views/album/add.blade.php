
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
    <title>POKEMON DATABASE</title>
    <link href="{{ URL::asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('assets/select2/css/select2.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ URL::asset('assets/css/album.css') }}" rel="stylesheet">
  </head>

  <body>

    @include('parts/header')

    <main role="main">

      <section class="jumbotron text-center">
        <div class="container">
          <img class="d-block mx-auto mb-4" src="{{ URL::asset('assets/images/logopokemon.png') }}" alt="" width="150" height="150">
        </div>
      </section>

      <div class="album py-5 bg-light">
        <div class="container">
			<form id="savePokemon" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Generation</label>
							<select class="form-control select2" name="generation" id="generation">
								<option></option>
								<option value="Generation I">Generation I</option>
								<option value="Generation II">Generation II</option>
								<option value="Generation III">Generation III</option>
								<option value="Generation IV">Generation IV</option>
								<option value="Generation V">Generation V</option>
								<option value="Generation VI">Generation VI</option>
								<option value="Generation VVI">Generation VII</option>
							</select>
						</div>
					</div>
	            </div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>Name</label>
							<input type="text" class="form-control" name="name" placeholder="Name" id="name">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Alias</label>
							<input type="text" class="form-control" name="alias" placeholder="Alias">
						</div>
					</div>
	            </div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Type</label>
							<select class="form-control select2" name="type[]" multiple>
								<option value="Bug">Bug</option>
								<option value="Dark">Dark</option>
								<option value="Dragon">Dragon</option>
								<option value="Electric">Electric</option>
								<option value="Fairy">Fairy</option>
								<option value="Fighting">Fighting</option>
								<option value="Fire">Fire</option>
								<option value="Flying">Flying</option>
								<option value="Fighting">Fighting</option>
								<option value="Ghost">Ghost</option>
								<option value="Grass">Grass</option>
								<option value="Ground">Ground</option>
								<option value="Ice">Ice</option>
								<option value="Normal">Normal</option>
								<option value="Poison">Poison</option>
								<option value="Psychic">Psychic</option>
								<option value="Rock">Rock</option>
								<option value="Steel">Steel</option>
								<option value="Water">Water</option>
							</select>
						</div>
					</div>
	            </div>
				<div class="row">
					<div class="col-md-2">
						<div class="form-group">
							<label>Attack</label>
							<input type="number" class="form-control" name="attack" placeholder="Attack">
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label>Defense</label>
							<input type="number" class="form-control" name="defense" placeholder="Defense">
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label>Stamina</label>
							<input type="number" class="form-control" name="stamina" placeholder="Stamina">
						</div>
					</div>
	            </div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Weakness</label>
							<select class="form-control select2" name="weakness[]" multiple>
								<option value="Bug">Bug</option>
								<option value="Dark">Dark</option>
								<option value="Dragon">Dragon</option>
								<option value="Electric">Electric</option>
								<option value="Fairy">Fairy</option>
								<option value="Fighting">Fighting</option>
								<option value="Fire">Fire</option>
								<option value="Flying">Flying</option>
								<option value="Fighting">Fighting</option>
								<option value="Ghost">Ghost</option>
								<option value="Grass">Grass</option>
								<option value="Ground">Ground</option>
								<option value="Ice">Ice</option>
								<option value="Normal">Normal</option>
								<option value="Poison">Poison</option>
								<option value="Psychic">Psychic</option>
								<option value="Rock">Rock</option>
								<option value="Steel">Steel</option>
								<option value="Water">Water</option>
							</select>
						</div>
					</div>
	            </div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Resistance</label>
							<select class="form-control select2" name="resistances[]" multiple>
								<option value="Bug">Bug</option>
								<option value="Dark">Dark</option>
								<option value="Dragon">Dragon</option>
								<option value="Electric">Electric</option>
								<option value="Fairy">Fairy</option>
								<option value="Fighting">Fighting</option>
								<option value="Fire">Fire</option>
								<option value="Fighting">Fighting</option>
								<option value="Ghost">Ghost</option>
								<option value="Grass">Grass</option>
								<option value="Ground">Ground</option>
								<option value="Ice">Ice</option>
								<option value="Normal">Normal</option>
								<option value="Poison">Poison</option>
								<option value="Psychic">Psychic</option>
								<option value="Rock">Rock</option>
								<option value="Steel">Steel</option>
								<option value="Water">Water</option>
							</select>
						</div>
					</div>
	            </div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Descriptions</label>
							<textarea class="form-control" name="descriptions" placeholder="Descriptions"></textarea>
						</div>
					</div>
	            </div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Photo</label>
							<input type="file" name="photo" class="form-control">
						</div>
					</div>
	            </div>
				<div class="row">
					<div class="col-md-6">
						<button class="btn btn-primary btn-lg btn-block" type="submit">Submit this new Pokemon</button>
					</div>
	            </div>
			</form>
        </div>
      </div>

    </main>

    @include('parts/footer')

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ URL::asset('assets/jquery/jquery.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/album/js/popper.min.js') }}"></script>
    <script src="{{ URL::asset('assets/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::asset('assets/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/album/js/holder.min.js') }}"></script>
	<script src="{{ URL::asset('assets/js/savepokemon.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		$('.select2').select2({
			placeholder: "Please choose!",
    		allowClear: true
		});
	</script>
  </body>
</html>
