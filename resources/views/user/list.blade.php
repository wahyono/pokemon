
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
    <title>POKEMON DATABASE</title>
    <link href="{{ URL::asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ URL::asset('assets/css/album.css') }}" rel="stylesheet">
  </head>

  <body>

    @include('parts/header')

    <main role="main">

      <section class="jumbotron text-center">
        <div class="container">
          <h1 class="jumbotron-heading">Pokemon Database</h1>
          <p class="lead text-muted">
			  Lorem ipsum dolor sit amet, consectetur adipiscing elit,
			  sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
			  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
			  nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
			  reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
			  pariatur. Excepteur sint occaecat cupidatat non proident,
			  sunt in culpa qui officia deserunt mollit anim id est laborum.
		  </p>
		  <p>
			  <a href="{{ url('user/add') }}" class="btn btn-primary">Add user</a>
		  </p>
        </div>
      </section>

      <div class="album py-5 bg-light">
        <div class="container">

          <div class="row">
			  <div class="col-md-6">
				  @if(count($data['users']) > 0)
				  <div class="card">
					  <div class="card-header">
						  <h5 class="card-title">Users</h5>
					  </div>
					  <div class="card-body">
						  <table class="table">
							  <thead>
								  <tr>
									  <th>#</th>
									  <th>Email</th>
									  <th>Name</th>
								  </tr>
							  </thead>
							  <tbody>
								@php $n = 1; @endphp
						  		@foreach($data['users'] as $users)
								<tr>
									<td style="width: 20px;">{{ $n++ }}</td>
									<td>{{ $users['email'] }}</td>
									<td>{{ $users['name'] }}</td>
								</tr>
						  		@endforeach
					  		  </tbody>
					  	  </table>
					  </div>
				  </div>
				  @else
				  <p>No users found</p>
				  @endif
			  </div>
          </div>
        </div>
      </div>

    </main>

    @include('parts/footer')

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ URL::asset('assets/jquery/jquery.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/album/js/popper.min.js') }}"></script>
    <script src="{{ URL::asset('assets/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/album/js/holder.min.js') }}"></script>
  </body>
</html>
