
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
    <title>POKEMON DATABASE</title>
    <link href="{{ URL::asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('assets/select2/css/select2.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ URL::asset('assets/css/album.css') }}" rel="stylesheet">
  </head>

  <body>

    @include('parts/header')

    <main role="main">

      <section class="jumbotron text-center">
        <div class="container">
          <img class="d-block mx-auto mb-4" src="{{ URL::asset('assets/images/logopokemon.png') }}" alt="" width="150" height="150">
        </div>
      </section>

      <div class="album py-5 bg-light">
        <div class="container">
			<form id="saveUser" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Email</label>
							<input type="email" class="form-control" name="email" placeholder="Email" id="email">
						</div>
					</div>
	            </div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Name</label>
							<input type="text" class="form-control" name="name" placeholder="Name" id="name">
						</div>
					</div>
	            </div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Password</label>
							<input type="password" class="form-control" name="password" placeholder="Password" id="password">
						</div>
					</div>
	            </div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Confirm Password</label>
							<input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password" id="confirm_password">
						</div>
					</div>
	            </div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Photo</label>
							<input type="file" name="photo" class="form-control">
						</div>
					</div>
	            </div>
				<div class="row">
					<div class="col-md-6">
						<button class="btn btn-primary btn-lg btn-block" type="submit">Submit this new User</button>
					</div>
	            </div>
			</form>
        </div>
      </div>

    </main>

    @include('parts/footer')

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ URL::asset('assets/jquery/jquery.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/album/js/popper.min.js') }}"></script>
    <script src="{{ URL::asset('assets/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::asset('assets/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/album/js/holder.min.js') }}"></script>
	<script src="{{ URL::asset('assets/js/saveuser.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		$('.select2').select2({
			placeholder: "Please choose!",
    		allowClear: true
		});
	</script>
  </body>
</html>
