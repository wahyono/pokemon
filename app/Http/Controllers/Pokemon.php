<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use App\Model\Pokemons;

class Pokemon extends BaseController {

	public function __construct(Request $request) {
		$this->request = $request;
	}

	public function index() {
		$data['pokemons'] = $this->allPokemons();

		return view('album.album', compact('data'));
	}

	public function details($id) {
		$data['id'] = $id;

		$get = Pokemons::where('id', $id)->first();

		$photo = url('assets/images/defaultpokemon.svg');

		if($get->photo != '') {
			if(file_exists($get->photo)) {
				$photo = url($get->photo);
			}
		}

		$data['photo'] = $photo;
		$data['details'] = $get;
		$data['weakness'] = explode(',', $get->weakness);
		$data['resistances'] = explode(',', $get->resistances);
		$data['type'] = explode(',', $get->type);
		$data['alias'] = ($get->alias == '' ? '-' : $get->alias);

		return view('album.details', compact('data'));
	}

	public function add() {
		return view('album.add');
	}

	public function edit($id) {
		$get = Pokemons::where('id', $id)->first();

		$data['details'] = $get;
		$data['weakness'] = explode(',', $get->weakness);
		$data['resistances'] = explode(',', $get->resistances);
		$data['type'] = explode(',', $get->type);
		$data['id'] = $id;

		return view('album.edit', compact('data'));
	}

	public function save() {
		if(!session('isLoggedIn')) {
			$response['status'] = 'session_expired';
			return response()->json($response);
	    }

		date_default_timezone_set("Asia/Jakarta");

		// get request
		$generation = $this->request->input('generation');
		$name = $this->request->input('name');
		$alias = $this->request->input('alias');
		$type = $this->request->input('type');
		$attack = $this->request->input('attack');
		$defense = $this->request->input('defense');
		$stamina = $this->request->input('stamina');
		$weakness = $this->request->input('weakness');
		$resistances = $this->request->input('resistances');
		$descriptions = $this->request->input('descriptions');
		$photo = $this->request->file('photo');

		// response
		$status = null;
		$urlPhoto = null;

		// file config
		if(isset($photo)) {
			$extension = strtolower($photo->getClientOriginalExtension());
			$extensions = array('jpg', 'jpeg', 'png');
			$path = 'uploads/photo/pokemon/';
			$uniq = uniqid();
			$filename = 'pokemon_'.$uniq.'.'.$extension;
			if(in_array($extension, $extensions)) {
				if(file_exists($path)) {
					$uploadSuccess = $photo->move($path, $filename);

					if($uploadSuccess) {
						$urlPhoto = $path.$filename;
					}
				}
			}
		}

		$pokemon = new Pokemons();
		$pokemon->generation = $generation;
		$pokemon->name = $name;
		$pokemon->alias = $alias;
		$pokemon->type = $this->explodeValue($type);
		$pokemon->attack = $attack;
		$pokemon->defense = $defense;
		$pokemon->stamina = $stamina;
		$pokemon->weakness = $this->explodeValue($weakness);
		$pokemon->resistances = $this->explodeValue($resistances);
		$pokemon->descriptions = $descriptions;
		$pokemon->photo = $urlPhoto;
		$pokemon->save();

		if($pokemon->save()) {
			$status = 'success';
		} else {
			$status = 'error';
		}

		$response['message'] = $status;
		$response['redirect'] = url('pokemon');

		return response()->json($response);
	}

	public function update() {
		if(!session('isLoggedIn')) {
			$response['message'] = 'session_expired';
			return response()->json($response);
	    }

		date_default_timezone_set("Asia/Jakarta");

		// get request
		$id = $this->request->input('reference');
		$generation = $this->request->input('generation');
		$name = $this->request->input('name');
		$alias = $this->request->input('alias');
		$type = $this->request->input('type');
		$attack = $this->request->input('attack');
		$defense = $this->request->input('defense');
		$stamina = $this->request->input('stamina');
		$weakness = $this->request->input('weakness');
		$resistances = $this->request->input('resistances');
		$descriptions = $this->request->input('descriptions');
		$photo = $this->request->file('photo');

		$get = Pokemons::where('id', $id);

		if($get->count() < 1) {
			$response['message'] = 'data_not_found';
			$response['redirect'] = url('pokemon');

			return response()->json($response);
		}

		$details = $get->first();

		// response
		$status = null;
		$urlPhoto = $details->photo;

		// file config
		if(isset($photo)) {
			$extension = strtolower($photo->getClientOriginalExtension());
			$extensions = array('jpg', 'jpeg', 'png');
			$path = 'uploads/photo/pokemon/';
			$uniq = uniqid();
			$filename = 'pokemon_'.$uniq.'.'.$extension;
			if(in_array($extension, $extensions)) {
				if(file_exists($path)) {
					$uploadSuccess = $photo->move($path, $filename);

					if($uploadSuccess) {
						$urlPhoto = $path.$filename;
					}
				}
			}
		}

		$pokemon = Pokemons::where('id', $id)->first();
		$pokemon->generation = $generation;
		$pokemon->name = $name;
		$pokemon->alias = $alias;
		$pokemon->type = $this->explodeValue($type);
		$pokemon->attack = $attack;
		$pokemon->defense = $defense;
		$pokemon->stamina = $stamina;
		$pokemon->weakness = $this->explodeValue($weakness);
		$pokemon->resistances = $this->explodeValue($resistances);
		$pokemon->descriptions = $descriptions;
		$pokemon->photo = $urlPhoto;
		$pokemon->save();

		if($pokemon->save()) {
			$status = 'success';
		} else {
			$status = 'error';
		}

		$response['message'] = $status;
		$response['redirect'] = url('pokemon/details/'.$id);

		return response()->json($response);
	}

	public function delete() {
		if(!session('isLoggedIn')) {
			$response['message'] = 'session_expired';
			return response()->json($response);
	    }

		$id = $_POST['reference'];

		$get = Pokemons::where('id', $id);

		if($get->count() < 1) {
			$response['message'] = 'data_not_found';
			$response['id'] = $id;
			$response['redirect'] = url('pokemon');

			return response()->json($response);
		}

		$delete = Pokemons::where('id', $id)->delete();

		if(!$delete) {
			$response['message'] = 'fail';
			$response['redirect'] = url('pokemon');
		} else {
			$response['message'] = 'success';
			$response['redirect'] = url('pokemon');
		}

		return response()->json($response);
	}

	private function allPokemons() {
		$get = Pokemons::where('id', '!=', null)->get(['id', 'photo', 'name', 'descriptions', 'created_at']);

		$response = array();

		if($get->count() > 0) {
			foreach($get as $pok) {
				$photo = url('assets/images/defaultpokemon.svg');

				if($pok->photo != '') {
					if(file_exists($pok->photo)) {
						$photo = url($pok->photo);
					}
				}

				array_push($response, array(
					'id' => $pok->id,
					'name' => $pok->name,
					'photo' => $photo,
					'descriptions' => substr($pok->descriptions, 0, 100).'...',
					'created_at' => date('d M Y H:i:s', strtotime($pok->created_at))
				));
			}
		}

		return $response;
	}

	private function explodeValue($value) {
		$response = null;

		if(!empty($value)) {
			foreach($value as $val) {
				if($response == null) {
					$response = $val;
				} else {
					$response = $response.','.$val;
				}
			}
		}

		return $response;
	}

}
