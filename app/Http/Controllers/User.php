<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Model\Users;
use Illuminate\Routing\Controller as BaseController;

class User extends BaseController {

	public function __construct(Request $request) {
		$this->request = $request;
	}

	public function index() {
		$data['users'] = $this->users();

        return view('user.list', compact('data'));
    }

    public function login() {
        return view('login.login');
    }

	public function add() {
        return view('user.add');
    }

    public function loginProcess() {
		$email = $this->request->input('email');
		$password = $this->request->input('password');

		$get = Users::where('email', $email);

		if($get->count() > 0) {
			if(md5($password) == $get->first()->password) {
				$data['message'] = 'success';
				$data['code'] = '003';

				$this->request->session()->put('isLoggedIn', true);
			} else {
				$data['message'] = 'wrong password';
				$data['code'] = '002';
			}
		} else {
			$data['message'] = 'user not found';
			$data['code'] = '001';
		}

		return response()->json($data);
    }

	public function logout() {
		Session::flush();

		return redirect('/');
	}

	public function save() {
		$email = $this->request->input('email');
		$name = $this->request->input('name');
		$password = $this->request->input('password');
		$photo = $this->request->file('photo');

		// response
		$status = null;
		$urlPhoto = null;

		// file config
		if(isset($photo)) {
			$extension = strtolower($photo->getClientOriginalExtension());
			$extensions = array('jpg', 'jpeg', 'png');
			$path = 'uploads/photo/user/';
			$uniq = uniqid();
			$filename = 'user_'.$uniq.'.'.$extension;
			if(in_array($extension, $extensions)) {
				if(file_exists($path)) {
					$uploadSuccess = $photo->move($path, $filename);

					if($uploadSuccess) {
						$urlPhoto = $path.$filename;
					}
				}
			}
		}

		$user = new Users();
		$user->email = $email;
		$user->name = $name;
		$user->password = md5($password);
		$user->photo = $urlPhoto;
		$user->save();

		if($user->save()) {
			$status = 'success';
		} else {
			$status = 'error';
		}

		$response['message'] = $status;
		$response['redirect'] = url('/');

		return response()->json($response);
	}

	private function users() {
		$get = Users::where('id', '!=', null)->get();

		$data = array();

		if($get->count() > 0) {
			foreach($get as $user) {
				array_push($data, array(
					'id' => $user->id,
					'name' => $user->name,
					'email' => $user->email
				));
			}
		}

		return $data;
	}

}
