<?php

namespace App\Http\Middleware;

use Closure;

class IsAuthComplete {
	public function handle($request, Closure $next) {
		if(!session('isLoggedIn')) {
			return redirect('/')->with('message_error', 'session expired');
		}

        return $next($request);
	}
}
