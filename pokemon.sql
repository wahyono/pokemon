-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 03, 2020 at 02:22 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pokemon`
--

-- --------------------------------------------------------

--
-- Table structure for table `pokemons`
--

CREATE TABLE `pokemons` (
  `id` bigint(20) NOT NULL,
  `generation` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `alias` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `attack` int(11) DEFAULT NULL,
  `defense` int(11) DEFAULT NULL,
  `stamina` int(11) DEFAULT NULL,
  `weakness` text,
  `resistances` text,
  `descriptions` text,
  `family` text,
  `photo` text,
  `thumbnail` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pokemons`
--

INSERT INTO `pokemons` (`id`, `generation`, `name`, `alias`, `type`, `attack`, `defense`, `stamina`, `weakness`, `resistances`, `descriptions`, `family`, `photo`, `thumbnail`, `created_at`, `updated_at`) VALUES
(14, 'Generation II', 'Chikorita', NULL, 'Grass', 92, 122, 128, 'Bug,Fire,Flying,Ice,Poison', 'Electric,Grass,Ground,Water', 'In battle, Chikorita waves its leaf around to keep the foe at bay. However, a sweet fragrance also wafts from the leaf, becalming the battling Pokémon and creating a cozy, friendly atmosphere all around.', NULL, 'uploads/photo/pokemon/pokemon_5e36858d9e84b.png', NULL, '2020-02-02 15:17:17', '2020-02-02 08:17:17');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `name` varchar(25) DEFAULT NULL,
  `email` text,
  `username` varchar(25) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `photo` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `photo`, `created_at`, `updated_at`) VALUES
(1, 'Angelcrusher', 'angelcrusher@gmail.com', 'pokemon', '00bfc8c729f5d4d529a412b12c58ddd2', NULL, '2020-02-02 09:46:26', '2020-02-02 02:50:23'),
(2, 'Wahyu Pamungkas', 'ipamseptember@gmail.com', NULL, '827ccb0eea8a706c4c34a16891f84e7b', 'uploads/photo/user/user_5e38193ca9ec5.jpg', '2020-02-03 12:59:40', '2020-02-03 05:59:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pokemons`
--
ALTER TABLE `pokemons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pokemons`
--
ALTER TABLE `pokemons`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
